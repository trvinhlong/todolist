import axios from 'axios'
import { BACKEND_SERVER } from '../constants'

export const getList = token => {
    return axios
        .get(`${BACKEND_SERVER}/api/task/list`, {
        headers: { 
            'Content-Type': 'application/json',
            'Authorization':token
        }
        })
        .then(res => {
            res.data.status = 'success'
            return res.data
        }).catch(err => {
            return {
                error:'Please login again!',
                status:'failed',
                message:err.message
            }
        })
}

export const addToList = task => {
  return axios
    .post(
      `${BACKEND_SERVER}/api/task`,
      {
        name: task.name,
        status: task.status
      },
      {
        headers: { 
            'Content-Type': 'application/json',
            'Authorization':task.token 
        }
      }
    )
    .then(function(response) {
        return response.data;
    }).catch(err => {
        return {
            error:'Error to add',
            status:'failed',
            message:err.message
        }
    })
}

export const deleteItem = (task, token) => {
  return axios
    .delete(`${BACKEND_SERVER}/api/task/${task}`, {
      headers: { 
            'Content-Type': 'application/json',
            'Authorization': token 
        }
    })
    .then(function(response) {
        console.log(response)
        return response;
    })
    .catch(function(error) {
      console.log(error)
      return error;
    })
}

export const updateItem = taskUpdateRequest => {
  return axios
    .put(
      `${BACKEND_SERVER}/api/task/${taskUpdateRequest.id}`,
      {
        name: taskUpdateRequest.name,
        status: taskUpdateRequest.status
      },
      {
        headers: { 
            'Content-Type': 'application/json', 
            'Authorization': taskUpdateRequest.token 
        }
      }
    )
    .then(function(response) {
        return response.data;
    })
}
